package com.bm.main.pos.callback;

public interface DialogCallback {
    void onSuccess();
    void onFailed();
}
