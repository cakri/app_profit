package com.bm.main.pos.ui

interface OnFragmentBackPressed {
    fun onFragmentBackPressed()
}