package com.bm.main.pos.callback;

public interface PermissionCallback {
    void onSuccess();
    void onFailed();
}
