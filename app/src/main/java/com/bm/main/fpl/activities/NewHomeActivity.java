package com.bm.main.fpl.activities;

import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;

import com.bm.main.pos.R;

public class NewHomeActivity extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_home);
    }
}
