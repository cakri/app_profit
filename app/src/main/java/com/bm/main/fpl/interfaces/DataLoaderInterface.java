package com.bm.main.fpl.interfaces;

import org.json.JSONObject;

public interface DataLoaderInterface
{
    void load(JSONObject request);
}
