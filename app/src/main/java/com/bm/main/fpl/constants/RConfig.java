package com.bm.main.fpl.constants;

public class RConfig {
    public static final String API_URL_FP = "remoteConfigApiUrlFP";
    public static final String API_URL_POS = "remoteConfigApiUrlPOS";
    public static final String API_URL_FP_DEVEL = "remoteConfigApiUrlFPDevel";
    public static final String API_URL_POS_DEVEL = "remoteConfigApiUrlPOSDevel";
    public static final String API_URL_FT = "remoteConfigApiUrlFT";
    public static final String API_YOU_TUBE = "remoteConfigApiYoutube";

//    public static final String URL_HELP = "remoteConfigUrlHelp";


    public static final String MIN_VERSION_CODE = "remoteConfigMinVersionCode";
    public static final String FORCE_UPDATE = "remoteConfigForceUpdate";
    public static final String ON_BOARD = "remoteConfigOnBoard";
    public static final String BIG_PROMO = "remoteConfigBigPromo";
    public static final String IS_BIG_PROMO = "remoteConfigIsBigPromo";
    public static final String IS_ToolTipAjakBisnis = "remoteConfigIsToolTipAjakBisnis";
    public static final String ContentToolTipAjakBisnis = "remoteConfigContentToolTipAjakBisnis";
    public static final String IS_PopupPromo = "remoteConfigIS_PopupPromo";
    public static final String UrlImgPopupPromo = "remoteConfigUrlImgPopupPromo";
    public static final String UrlCTAPopupPromo = "remoteConfigUrlCTAPopupPromo";
    public static final String DateRangeMutasi = "remoteConfigDateRangeMutasi";
    public static final String DateRangeReport = "remoteConfigDateRangeReport";
    public static final String DateRangeKomisi = "remoteConfigDateRangeKomisi";
    public static final String ListBank="remoteConfigListBank";
    public static final String IS_PROMO_BUTTON_SHOW = "remoteConfigIsPromoButtonShow";
    public static final String DURASIBIGPROMO = "remoteConfigDurasiBigPromo";
    public static final String ADD_MENU ="remoteConfigAddMenu" ;
}
