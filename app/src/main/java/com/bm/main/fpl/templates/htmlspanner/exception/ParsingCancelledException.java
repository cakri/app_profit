package com.bm.main.fpl.templates.htmlspanner.exception;

/**
  Simple RuntimeException which is throws when parsing is cancelled by the user.

 @author Alex Kuiper
 */

public class ParsingCancelledException extends RuntimeException {
}
