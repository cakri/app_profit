package com.bm.main.single.ftl.train.constants;

/**
 * Created by sarifhidayat on 2/6/19.
 **/
public class TrainKeyPreference {

    public static final  String stationListData ="stationListData";

    public static final String stationKotaAsal="stationKotaAsal";

    public static final String stationNamaAsal="stationNamaAsal";
    public static final String stationKodeAsal="stationKodeAsal";
    public static final String stationKotaTujuan="stationKotaTujuan";
    public static final String stationKodeTujuan="stationKodeTujuan";
    public static final String stationNamaTujuan="stationNamaTujuan";
    public static final String departureDateTrain="departureDateTrain";
    public static final String departureDateShowTrain="departureDateShowTrain";
    public static final String returnDateTrain="returnDateTrain";
    public static final String returnDateShowTrain="returnDateShowTrain";

    public static final String countAdultTrain="countAdultTrain";
    public static final String countChildTrain="countChildTrain";
    public static final String countInfantTrain="countInfantTrain";


    public static final String jmlpenumpang="jmlpenumpang";
    public static final String classes="classes";
    public static final String schedule_list_train_default="schedule_list_train_default";
    public static final String trainFilterModel="trainFilterModel";
    public static final String listKeretaFilter="listKeretaFilter";
    public static final String schedule_list_train="schedule_list_train";
    public static final String onClickSchedule="onClickSchedule";
    public static final String nominalAdmin="nominalAdmin";
    public static final String trainNumber="trainNumber";
    public static final String grade="grade";
    public static final String trainName="trainName";
    public static final String arrivalTime="arrivalTime";
    public static final String priceAdult="priceAdult";
    public static final String arrDateTrain="arrDateTrain";
    public static final String depDateTrain="depDateTrain";
    public static final String departureTime="departureTime";
    public static final String bookResult="bookResult";
    public static final String isNewTrain="isNewTrain";

}
